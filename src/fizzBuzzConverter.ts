export type FizzBuzzType = number | string;


const fizzBuzzConverter = (number: number): FizzBuzzType => {
    // 1.打印字符串"Fizz" - Given 一个数字 - When 该数字为 3 的倍数 - then 打印 "Fizz"
    // 2.打印字符串"Buzz" - Given 一个数字 - When 该数字为 5 的倍数 - then 打印 "Buzz"
    // 3.打印字符串"FizzBuzz" - Given 一个数字 - When 该数字为 15 的倍数 - then 打印 "FizzBuzz"
    // 4.打印数字字符串 - Given 一个数字 - When 该数字不是 3、5、15 的倍数 - then 打印数字本身
    // 5.异常处理 - Given 一个数字 - When 不在 1~100 之间 - then 抛出异常
    // 6.文档中补录内容：如果所报数字包含了3，那么也不能说该数字，而是要说相应的单词，比如要 报13的同学应该说Fizz。
      // 如果数字中包含了3，那么忽略规则2和规则3，比如要报30的同学只报Fizz，不报FizzBuzz 。
    // 7.文档补录内容：如果同时是三个特殊数的倍数， 那么要说FizzBuzzWhizz。  
    // 8.文档补录内容： 如果所报数字是第7的倍数，那么要说Whizz
   
    //   由此总结6的优先级大于2、3

    if (number<1 || number>100) {
       throw new RangeError("参数必须是在1-100之间的数字!");  
    } else if(String(number).indexOf('3')!==-1){
        return 'Fizz'
    } else if(number % 5 == 0 && number % 3 == 0 &&number % 7 == 0){
        return 'FizzBuzzWhizz'
    }else if(number % 15 == 0) {
        return 'FizzBuzz'
    } else if (number % 3 == 0) {
        return 'Fizz'
    } else if (number % 5 == 0){
        return 'Buzz'
    } else if(number % 7 == 0){
        return 'Whizz'
    } else {
        return String(number)
    }
}

export default fizzBuzzConverter;